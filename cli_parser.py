# CLI parser module.
# Author: Eduard Constantinescu - s192629

import argparse


def parse_cli():
    """
    # Parse CLI arguments. See READ.ME file for more information on CLI usage.

    # Returns:
    #   args: CLI parsed arguments
    """
    parser = argparse.ArgumentParser(description='Eduard\'s TLS Scanner')
    parser.add_argument('-b', '--blocklist', help='Path to blocklist', required=False)
    parser.add_argument('-i', '--input', help='Path to input file', required=False)
    parser.add_argument('-c', '--certificate', help='Path to trusted root store', required=False)
    parser.add_argument('-s', '--scan', help='Start the scan', action='store_true')
    parser.add_argument('-a', '--analyse', help='Start the analysis', action='store_true')
    parser.add_argument('-v', '--verbose', help='Increase verbosity', action='store_true')

    args = parser.parse_args()

    if not (args.scan or args.analyse):
        parser.error('Select the mode of operation: -s or -a!')

    if args.scan:
        if not args.certificate:
            parser.error('Can\'t start scan! Select a trusted root store using -c or --certificate!')
        if not args.input:
            parser.error('Can\'t start scan! Select an input file using -i or --input!')
        if not args.blocklist:
            parser.error('Can\'t start scan! Select a blocklist file using -b or --blocklist!')

    return args
