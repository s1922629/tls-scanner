# IO module.
# Author: Eduard Constantinescu - s1922629

import csv
import json
import helpers
import OpenSSL
from tabulate import tabulate
from filelock import Timeout, FileLock


scanner_output_file = 'scanner_output.jsonl'
scanner_output_lock = 'scanner_output.jsonl.lock'


def read_input_file(file_path):
    """
    # Read the input file used for scanning TLS connections. Store data in a {host: ip} type dictionary.

    # Parameters:
    #   file_path (str): Path to the input file.

    # Returns:
    #   input_dict (dict): A {host: ip} dictionary.
    """
    input_dict = {}
    with open(file_path, 'r') as input_file:
        csv_reader = csv.reader(input_file)
        for line in csv_reader:
            input_dict[line[0]] = line[1]
    return input_dict


def write_output_file(tls_connection):
    """
    # Append the results of a single TLS connection to the output file.
    # We use FileLocks to ensure no race-conditions occur when 2 threads try to append to the file at the same time.
    # The output file is .jsonl for easier parsing when analyzing.

    # Parameters:
    #   tls_connection (TLS_connection): Object storing all the information of the finished TLS connection.
    """
    lock = FileLock(scanner_output_lock, timeout=1)
    with lock:
        with open(scanner_output_file, 'a') as output_file:
            jsonObj = json.dumps(tls_connection.__dict__)
            output_file.write(jsonObj + '\n')


def read_blocklist(file_path):
    """
    # Read the blocklist file used to skip certain hosts / IPs when scanning.
    # Store the data in a dictionary containing a 'hosts' list and an 'ips' list.

    # Parameters:
    #   file_path (str): Path to the blocklist file.

    # Returns:
    #   blocklist (dict): A dictionary containing all the blocked hosts and IPv4 addresses.
    """
    blocklist = {
        'hosts': [],
        'ips': []
    }
    with open(file_path, 'r') as blocklist_file:
        for line in blocklist_file:
            stripped = line.rstrip('\n')
            if helpers.valid_ip(stripped):
                blocklist['ips'].append(stripped)
            else:
                blocklist['hosts'].append(stripped)
    return blocklist


def read_analysis_file():
    """
    # Read the TLS scanner output .jsonl file used for the analysis. Store each JSON object in a list.
    
    # Returns:
    #   analysis_data (list): A list of JSON object representing TLS scan data. 
    """
    analysis_data = []
    with open('scanner_output.jsonl', 'r') as analysis_file:
        json_lines = analysis_file.readlines()
        for line in json_lines:
            analysis_data.append(json.loads(line))
    return analysis_data


def write_analysis_result(title, header, data):
    """
    # Append an analysis result table to the output file.

    # Parameters:
    #   title (str): Title of the result table.
    #   header (list[str]): List of table header names.
    #   data (list): List of row values.
    """
    with open('analysis_output', 'a') as analysis_file:
        analysis_file.write(title + '\n')
        analysis_file.write(tabulate(data, headers=header))
        analysis_file.write('\n\n')


def read_public_ct_logs():
    """
    # Read the publicly available CT log information: description and base64-encoded LogIDs.

    # Returns:
    #   ct_logs: List of JSON objects represnting CT logs.
    """
    ct_logs = []
    with open('ct_logs.jsonl', 'r') as ct_logs_file:
        json_lines = ct_logs_file.readlines()
        for line in json_lines:
            ct_logs.append(json.loads(line))
    return ct_logs


def read_trusted_certificates(file_path):
    """
    # Read and store trusted CAs from a file.

    # Parameters:
    #   file_path (str): Path to trusted CAs file.

    # Returns:
    #   root_store: An OpenSSL root store object where trusted certificates are stored.
    """
    start_line = b'-----BEGIN CERTIFICATE-----'
    root_store = OpenSSL.crypto.X509Store()

    with open(file_path, 'rb') as pem_file:
        pem_data = pem_file.read()
        for cert in pem_data.split(start_line)[1:]:
            trusted_cert = OpenSSL.crypto.load_certificate(OpenSSL.crypto.FILETYPE_PEM, start_line + cert)
            root_store.add_cert(trusted_cert)

    return root_store