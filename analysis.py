# TLS analysis module.
# Author: Eduard Constantinescu - s1922629

from functools import reduce
import io_utils
import logging
from helpers import hex_to_base64

logger = logging.getLogger('ANALYSIS')


def tls_analysis(tls_results):
    """
    # Analyse TLS related data. Output in a pretty table format.

    # Parameters:
    #   tls_results: Dictionary containing TLS versions and their occurrences.
    """
    total_tls = reduce(lambda x, y: x + y, [tls_results[i] for i in tls_results])

    tab_title = 'TLS Version Analysis'
    tab_headers = ['TLS_version', 'No. of occurrences', '% of occurrences']
    tab_data = [
        [version, tls_results[version], tls_results[version] * 100 / total_tls] for version in tls_results
    ]

    io_utils.write_analysis_result(tab_title, tab_headers, tab_data)


def cert_analysis(cert_results):
    """
    # Analyse certificate related data. Output in a pretty table format.

    # Parameters:
    #   cert_results: Dictionary containing counts of valid and invalid certificates.
    """
    total_certs = cert_results['valid'] + cert_results['invalid']

    tab_title = 'Certificate Validity Analysis'
    tab_headers = ['Certificate status', 'No. of occurrences', '% of occurrences']
    tab_data = [
        ['Valid', cert_results['valid'], cert_results['valid'] * 100 / total_certs],
        ['Invalid', cert_results['invalid'], cert_results['invalid'] * 100 / total_certs]
    ]

    io_utils.write_analysis_result(tab_title, tab_headers, tab_data)


def ca_analysis(ca_results):
    """
    # Analyse CA related data. Output in a pretty table format.

    # Parameters:
    #   ca_results: Dictionary containing organization names and the count of their occurrences.
    """
    ca_list = [(ca, ca_results[ca]) for ca in ca_results]
    ca_list.sort(key=lambda x: x[1], reverse=True)

    total_ca = reduce(lambda x, y: x + y, [i[1] for i in ca_list])

    tab_title = 'CA Analysis'
    tab_headers = ['CA', 'No. of occurrences', '% of occurrences']
    tab_data = [
        [ca[0], ca[1], int(ca[1]) * 100 / total_ca] for ca in ca_list[:10]
    ]

    io_utils.write_analysis_result(tab_title, tab_headers, tab_data)


def ct_log_analysis(ct_log_results):
    """
    # Analyse CT log data. Output in a pretty table format.
    # If publicly available CT log information is loaded, also match CT LogIDs with CT log names.

    # Parameters:
    #   ct_log_results: Dictionary containg LogIDs and the count of their occurrences.
    """
    try:
        # Load publicly available CT logs information.
        public_ct_data = io_utils.read_public_ct_logs()
    except Exception as e:
        logger.error('Failed to load public CT logs data!')
        logger.debug(type(e), e)

    ct_log_list = [[ct_log, ct_log_results[ct_log]] for ct_log in ct_log_results]

    if public_ct_data:
        for ct_log in ct_log_list:
            # Convert CT LogIDs from hex to base64.
            log_id_64 = hex_to_base64(ct_log[0].replace(':', ''))
            name = '-'
            for log in public_ct_data:
                if log['log_id'] == log_id_64:
                    name = log['description']
                    break
            ct_log.append(name)

    ct_log_list.sort(key=lambda x: x[1], reverse=True)

    total_ct_logs = reduce(lambda x, y: x + y, [i[1] for i in ct_log_list])

    tab_title = 'CT Logs Analysis'
    tab_headers = ['CT LogID', 'CT Log Name', 'No. of occurrences', '% of occurrences']
    tab_data = [
        [ct_log[0], ct_log[2], ct_log[1], int(ct_log[1]) * 100 / total_ct_logs] for ct_log in ct_log_list
    ]

    io_utils.write_analysis_result(tab_title, tab_headers, tab_data)

def parse_analysis(analysis_data):
    """
    # Analyse TLS scanner output data. Creates some pretty tables.

    # Parameters:
    #   analysis_data: JSON objects representing TLS scanning outputs.
    """
    tls_results = {
        'TLSv1': 0,
        'TLSv1.1': 0,
        'TLSv1.2': 0,
        'TLSv1.3': 0
    }

    cert_results = {
        'valid': 0,
        'invalid': 0
    }

    ca_results = {}

    ct_log_results = {}

    # Iterate through each TLS result and store relevant data.
    for tls_entry in analysis_data:

        # Count TLS versions of successful TLS sessions.
        if tls_entry['successful_TLS']:
            tls_results[tls_entry['TLS_version']] += 1

        # Count verified certificate chains (and hostname checks).
        if tls_entry['verified_cert']:
            cert_results['valid'] += 1

            # Countr CA organization occurrences.
            organization = tls_entry['issuer']['O']
            if not ca_results.get(organization):
                ca_results[organization] = 0
            ca_results[organization] += 1
        else:
            cert_results['invalid'] += 1

        # Store CT log data.
        if tls_entry['ct_logs']:
            for log in tls_entry['ct_logs']:
                log_id = log['logID']
                if not ct_log_results.get(log_id):
                    ct_log_results[log_id] = 0
                ct_log_results[log_id] += 1

    # Parse data and create some pretty tables.
    try:
        tls_analysis(tls_results)
        cert_analysis(cert_results)
        ca_analysis(ca_results)
        ct_log_analysis(ct_log_results)
    except Exception as e:
        logger.debug(type(e), e)
        logger.error('Error parsing analysis input file! Aborting...')