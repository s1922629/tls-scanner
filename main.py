# TLS Scanner
# Main module
# Author: Eduard Constantinescu - s1922629

import logging
from cli_parser import parse_cli
import tls
import analysis
import io_utils
import time

logger = logging.getLogger('MAIN')


def setup_loggers(verbose=False):
    """
    # Setup loggers.
    
    # Parameters:
    #   verbose (bool): Enable verbose logging. Default set to False.
    """
    if verbose:
        logging.basicConfig(format="%(asctime)s %(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info("Verbose output enabled!")
    else:
        logging.basicConfig(format="%(asctime)s %(levelname)s: %(message)s", level=logging.INFO)


def scan(args):
    """
    # Start the TLS scanning.

    # Parameters:
    #   args: CLI parsed arguments.
    """
    try:
        logger.info('Loading input data...')
        input_data = io_utils.read_input_file(args.input)
    except Exception as e:
        logger.debug(type(e), e)
        logger.error('Failed to load input data! Aborting...')
        exit(1)

    try:
        logger.info('Loading blocklist...')
        blocklist = io_utils.read_blocklist(args.blocklist)
    except Exception as e:
        logger.debug(type(e), e)
        logger.error('Failed to load blocklist! Aborting...')
        exit(1)

    try:
        logger.info('Loading root store...')
        root_store = args.certificate
    except Exception as e:
        logger.debug(type(e), e)
        logger.error('Failed to load root store! Aborting...')
        exit(1)

    # Open the output file in the beginning so we can append to it later during the scan.
    with open('scanner_output.jsonl', 'w'):
        pass

    now = time.time()
    logger.info('Starting scanning!')

    tls.scan(input_data, blocklist, root_store)

    end = time.time()
    logger.info('Finished scanning in {} seconds!'.format(end - now))


def analyse(args):
    """
    # Start the TLS analysis. Requires a TLS scan output file to exist.
    
    # Parameters:
    #   args: CLI parsed arguments.
    """
    try:
        logger.info('Loading input analysis file...')
        analysis_data = io_utils.read_analysis_file()
    except Exception as e:
        logger.debug(type(e), e)
        logger.error('Failed to load input analysis file! Aborting...')
        exit(1)

    # Open the output file so we can append to it later during the analysis.
    with open('analysis_output', 'w'):
        pass

    now = time.time()
    logger.info('Starting analysis!')

    analysis.parse_analysis(analysis_data)

    end = time.time()
    logger.info('Finished analysis in {} seconds!'.format(end - now))


def main():
    args = parse_cli()

    setup_loggers(args.verbose)

    if args.scan:
        scan(args)
    if args.analyse:
        analyse(args)


if __name__ == '__main__':
    main()