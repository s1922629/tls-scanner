# TLS Scanner
## Course: Empirical Security Analysis Engineering - October 2022
### Author: Eduard Constantinescu
### Student Number: s1922629

## Description

This is a minimalistic TLS scanner and analyser written in Python 3.9. It attempts to create TLS connections, retrieve certificate information, and parse and analyse data in a human readable format.

## Requirements

- Python 3.9
- Pipenv

## Installation

Run the following command to create a new Python virtual environment and install all the dependencies:

```bash
$ pipenv install
```

## Usage

To use the application after installation, first activate the virtual environment:

```bash
$ pipenv shell
```

The tool supports CLI usage:

```bash
$ python3 main.py -h

usage: main.py [-h] [-b BLOCKLIST] [-i INPUT] [-c CERTIFICATE] [-s] [-a] [-v]
```

Description of CLI flags:

- `-h`: Output CLI usage to console and exit.
- `-b, --blocklist`: (**REQUIRED**) Path to blocklist CSV file. Hosts read from this file will be skipped in the scanning process. Add your entries as pairs of `HOST,IPV4`. 
- `-i, --input`: (**REQUIRED**) Path to input CSV file. Hosts read from this file will be scanned if they are not found in the blocklist file. Add your entries as pairs of `HOST,IPV4`.
- `-c, --certificate`: (**REQUIRED**) Path to trusted CA PEM file. Certificates read from this file will be used to verify the certificates of scanned hosts.
- `-s`: Perform a TLS scan using the input file, blocklist file and trusted certificates files. Results will be saved to the `scanner_output.jsonl` file.
- `-a`: Perform a TLS analysis on a `scanner_output.jsonl` file. Results will be saved to the `analysis_output` file.
- `-v`: Enable verbose mode.

### Examples

Run a TLS scan + analysis using verbose mode:

```bash
$ python3 main.py -b week3-blocklist.txt -i assignment-week3-input.csv -c week3-roots.pem -s -a -v
```

Run just the analysis, granted that the `scanner_output.jsonl` file exists:

```bash
$ python3 main.py -a
```