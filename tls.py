# TLS scanning module
# Author: Eduard Constantinescu - s1922629

import OpenSSL, ssl, socket
import helpers
import logging
import io_utils
import concurrent.futures
from tqdm import tqdm

logger = logging.getLogger('SCANNER')


class TLS_connection:
    """
    # Class which can perform TLS scanning and can store the information of a single TLS connection.
    """
    def __init__(self, host: str, ip: str):
        self.host = host
        self.ip = ip

        self.successful_TLS = None
        self.TLS_version = None
        self.verified_cert = None
        self.ct_logs = []
        self.issuer = None
        self.notAfter = None
        self.notBefore = None
        self.signatureAlgorithm = None
        self.subject = None

    def get_certificate(self, root_store):
        """
        # Creates a TLS connection with a remote host / IPv4 address.
        # Attempts to retrieve and verify the server's certificate(s).

        # Parameters:
        #   root_store (str): Path to trusted CAs file.
        """

        # Create an SSL context and inject trusted CAs.
        context_verify_certs = ssl.SSLContext(ssl.PROTOCOL_TLS_CLIENT)
        context_verify_certs.load_verify_locations(root_store)

        logger.debug('Attempting TLS connection with host {} at {}...'.format(self.host, self.ip))

        try:
            # First open a socket and create a connection to the host on port 443.
            with socket.create_connection((self.ip, 443), timeout=5) as sock:
                try:
                    # Wrap the socket in an SSL context that verifies the certificate chain.
                    self._context_connection(context_verify_certs, sock)
                    self.verified_cert = True
                    self.log_to_file()
                except ssl.SSLCertVerificationError:
                    logger.debug('Failed certificate verification for host {} at {}!'.format(self.host, self.ip))

                    self.verified_cert = False

                    # If certificate validation failed, create a new context which does not validate or checks hosts.
                    context_no_verify = ssl.SSLContext()
                    context_no_verify.check_hostname = False
                    context_no_verify.verify_mode = ssl.CERT_NONE

                    # Open a new socket and create a new connection to the host on port 443.
                    with socket.create_connection((self.ip, 443), timeout=5) as sock_no_verify:
                        # Wrap the socket in an SSL context that does not verify certificates or check hosts.
                        self._context_connection(context_no_verify, sock_no_verify)
                        self.log_to_file()
                except (ssl.SSLEOFError, ssl.SSLError):
                    logger.warning('Failed TLS connection for host {} at {}!'.format(self.host, self.ip))
        except socket.timeout:
            logger.warning('Timed out for host {} at {}!'.format(self.host, self.ip))
        except Exception as e:
            logger.debug(type(e), e)

    def _context_connection(self, context, sock):
        """
        # Wrap the open socket in a context to retrieve TLS information and server certificates.

        # Parameters:
        #   context (ssl.SSLContext): SSL context to wrap the socket in.
        #   sock (ssl.Socket): Open socket used to communicate with the remote host.
        """
        with context.wrap_socket(sock, server_hostname=self.host) as ssock:
            logger.debug('Successful TLS connection with host {} at {}...'.format(self.host, self.ip))
            self.successful_TLS = True
            self.TLS_version = ssock.version()

            # Retrieve the host's x509 certificate(s) and parse them for future analysis..
            certificate = pem_convert_x509(ssl.DER_cert_to_PEM_cert(ssock.getpeercert(True)))
            self.parse_certificate(certificate)

    def parse_certificate(self, certificate):
        """
        # Parse an x509 certificate for future analysis.

        # Parameters:
        #   certificate: x509 certificate.
        """
        self.issuer = {i[0].decode(): i[1].decode() for i in certificate.get_issuer().get_components()}
        self.notAfter = helpers.parse_z_timestamp(certificate.get_notAfter().decode())
        self.notBefore = helpers.parse_z_timestamp(certificate.get_notBefore().decode())
        self.signatureAlgorithm = certificate.get_signature_algorithm().decode()
        self.subject = {i[0].decode(): i[1].decode() for i in certificate.get_subject().get_components()}

        # Parse CT logs.
        for i in range(certificate.get_extension_count()):
            extension = str(certificate.get_extension(i))
            if 'Signed Certificate Timestamp' in str(extension):
                splitted = extension.split('Signed Certificate Timestamp:')
                for _ in splitted[1:]:
                    ct_log = {}
                    ct_log['version'] = _.split('Version')[1].split(': ')[1].split('\n')[0]
                    ct_log['logID'] = _.split('Log ID')[1].split(': ')[1].split('Timestamp')[0].replace(' ', '').replace('\n', '')
                    ct_log['timestamp'] = _.split('Timestamp')[1].split(': ')[1].split('\n')[0]
                    ct_log['extensions'] = _.split('Extensions')[1].split(': ')[1].split('\n')[0]

                    ct_log['signature'] = {}
                    ct_log['signature']['algorithm'] = _.split('Signature')[1].lstrip(' ').split(' ')[1].replace('\n', '')
                    ct_log['signature']['value'] = _.split(ct_log['signature']['algorithm'])[1].replace(' ', '').replace('\n', '')
                    self.ct_logs.append(ct_log)

    def log_to_file(self):
        """
        # Append the TLS connection data to the output file.
        """
        io_utils.write_output_file(self)


def pem_convert_x509(pem_certificate):
    """
    # Convert a PEM certificate to an x509 format.

    # Parameter:
    #   pem_certificate: PEM formatted certificate.

    # Returns:
    #   An x509 encoding of the input PEM certificate.
    """
    return OpenSSL.crypto.load_certificate(OpenSSL.crypto.FILETYPE_PEM, str.encode(pem_certificate))


def scan(input_data, blocklist, root_store):
    """
    # Starts the TLS scanning process.
    # Uses 5 concurrent threads to perform scans more efficiently.
    
    # Parameters:
    #   input_data (dict): A {host: ip} dictionary of input to be scanned.
    #   blocklist (dict): A dictionary with a list of hosts and a list of IPs to be skipped during scanning.
    #   root_store (str): Path to the file of trusted CAs.
    """
    tls_connections = []
    
    # Iterate through input data, skipping blocked hosts / IPs and create TLS_connection objects for each one to be scanned.
    for host, ip in input_data.items():
        if not helpers.is_blocked(host, ip, blocklist):
            tls_conn = TLS_connection(host=host, ip=ip)
            tls_connections.append(tls_conn)
        else:
            logger.warning('Skipping blocked scan for {} {}'.format(host, ip))

    # Create a ThreadPool with 5 workers and start scanning using the previously created TLS_connection objects.
    with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor:
        future_to_tls = {executor.submit(tls_conn.get_certificate, root_store): tls_conn for tls_conn in tls_connections}
        
        # Use tqdm bar to visualize progress.
        with tqdm(total=len(tls_connections)) as bar:
            for future in concurrent.futures.as_completed(future_to_tls):
                tls_conn = future_to_tls[future]
                try:
                    bar.update(1)
                    done = future.result()
                except Exception as e:
                    logger.error('Error during thread execution!  ', tls_conn)
