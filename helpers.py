# Helper module, includes several methods useful for scanning and analysis.
# Author: Eduard Constantinescu - s1922629

import re
import ipaddress
import codecs


def valid_ip(ip):
    """
    # Check if the input is a valid IPv4 address using regex rules.

    # Parameters:
    #   ip (str): The input IPv4 address to be checked.

    # Returns:
    #   True, if the input is a valid IPv4 address.
    #   False, otherwise.
    """
    match_rule = re.search(r'^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})(\/\d{1,2})?$', ip)
    if match_rule:
        groups = match_rule.groups()
        for i in groups[:-1]:
            if int(i) > 255:
                return False
        if groups[-1] and int(groups[-1][1:]) > 32:
            return False
        return True
    return False


def is_blocked(host, ip, blocklist):
    """
    # Check if the input host or IP is on a given blocklist. Used to exclude such hosts / IPs from scans.
    
    # Parameters:
    #   host (str): Host to be checked.
    #   ip (str): IPv4 address to be checked.
    #   bocklist (dict): Dictionary containing a list of blocked hosts and a list of blocked IPs.

    # Returns:
    #   True, if the host or IP is on the blocklist.
    #   False, otherwise.
    """
    if host in blocklist['hosts']:
        return True
    for subnet in blocklist['ips']:
        if ipaddress.ip_address(ip) in ipaddress.ip_network(subnet):
            return True
    return False


def parse_z_timestamp(timestamp):
    """
    # Pretty parse a "z-timestamp" into a more human readable timestamp. This z-timestamp format is retrieved when parsing a TLS certificate. 
    
    # Parameters:
    #   timestamp (str): Timestamp of a TLS certificate.

    # Returns:
    #   A human readable timestamp in the foramt "YYYY-MM-DD-hh-mm-ss"
    """
    return timestamp[:4] + '-' + timestamp[4:6] + '-' + timestamp[6:8] + '-' + timestamp[8:10] + '-' + timestamp[10:12] + '-' + timestamp[12:14]


def hex_to_base64(value):
    """
    # Convert a hexadecimal encoded CT LogID into a base64 format. Useful for matching scanned CT LogIDs to the CT description file, thus retrieving a CT log's name.

    # Parameter:
    #   value (str): Hexadecimal encode CT LogID, e.g. 1A2B3C ... FF

    # Returns:
    #   The base64 encoded CT LogID.
    """
    return codecs.encode(codecs.decode(value, 'hex'), 'base64').decode()[:-1]